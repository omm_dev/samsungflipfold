using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandlePhoneType : MonoBehaviour
{
    public GameObject gridManagerFlip;
    public GameObject gridManagerFold;

    public GameObject[] component_TileDemoFlip;
    public GameObject[] component_TileDemoFold;
    public GameObject DisableForUI;

    public bool ForceFlip = false;

    // Start is called before the first frame update
    void Start()
    {
        ScreenManager m_screenManager = FindObjectOfType<ScreenManager>();
        if (Screen.width == 1080 || ForceFlip == true)
        {
            gridManagerFlip.SetActive(true);
            foreach (GameObject tileDemo in component_TileDemoFlip)
            {
                tileDemo.SetActive(true);
            }
            gridManagerFold.SetActive(false);
            foreach (GameObject tileDemo in component_TileDemoFold)
            {
                tileDemo.SetActive(false);
            }
        }
        else
        {
          //  DisableForUI.SetActive(false);
            gridManagerFold.SetActive(true);
            foreach (GameObject tileDemo in component_TileDemoFold)
            {
                tileDemo.SetActive(true);
            }
            gridManagerFlip.SetActive(false);
            foreach (GameObject tileDemo in component_TileDemoFlip)
            {
                tileDemo.SetActive(false);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
