using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChangeCounterRestart : MonoBehaviour
{
    public TextMeshProUGUI textMeshPro;
    public int counter;
    private int initialCounter;

    // Start is called before the first frame update

    private void Start()
    {
        textMeshPro.text = counter.ToString();
        initialCounter = counter;
    }
    private void OnEnable()
    {
     //   EventManager.StartListening(EventNames.ResetARGame, RestartGame);
        EventManager.StartListening(EventNames.ResetAll, ResetAll);
    }

    private void OnDisable()
    {
      //  EventManager.StopListening(EventNames.ResetARGame, RestartGame);
        EventManager.StopListening(EventNames.ResetAll, ResetAll);

    }

    public void RestartGame()
    {
        if (counter > 0)
        {
            counter--;
            textMeshPro.text = counter.ToString();
            EventManager.TriggerEvent(EventNames.ResetARGame);
        }
    }


    private void ResetAll()
    {
        counter = initialCounter;
        textMeshPro.text = counter.ToString();
    }
}
