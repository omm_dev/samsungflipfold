using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using TouchScript.Hit;


public class BalanceTile : MonoBehaviour
{
    public Transform rotateAnchor;
    public float m_rotationTime = 1f;
    public float m_rotationAngle = 45f;

    private Quaternion initialRotation;
    private bool isRotating = false;
    private float elapsedTime = 0;


    private void OnEnable()
    {
        GetComponent<TapGesture>().Tapped += tappedHandler;
        EventManager.StartListening(EventNames.ResetARGame, ResetTiles);
    }

    private void OnDisable()
    {
        GetComponent<TapGesture>().Tapped -= tappedHandler;
        EventManager.StopListening(EventNames.ResetARGame, ResetTiles);
    }

    private void Start()
    {
        initialRotation = this.rotateAnchor.rotation;
    }

    private void ResetTiles()
    {
        this.rotateAnchor.rotation = initialRotation;
    }

    private void tappedHandler(object sender, EventArgs e)
    {
        if (isRotating == false && ((m_rotationAngle < 0 && Mathf.Approximately(this.rotateAnchor.eulerAngles.y, 330) || m_rotationAngle > 0 && Mathf.Approximately(this.rotateAnchor.eulerAngles.y, 30))))
        {
            elapsedTime = 0;
            StartCoroutine(RotateTileCoroutine(this.rotateAnchor.rotation));
        }
    }

    IEnumerator RotateTileCoroutine(Quaternion originalRotation)
    {
        Quaternion targetRotation = originalRotation;
        targetRotation *= Quaternion.Euler(m_rotationAngle, 0, 0);
        while (elapsedTime < m_rotationTime)
        {
            elapsedTime += Time.deltaTime;

            isRotating = true;

            this.rotateAnchor.rotation = Quaternion.Slerp(originalRotation, targetRotation, elapsedTime / m_rotationTime);
            yield return new WaitForEndOfFrame();
        }
        isRotating = false;

    }
}
