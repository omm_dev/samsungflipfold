using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using TouchScript.Hit;

public class RotateTile : MonoBehaviour
{
    public float m_rotationTime = 1f;
    public float m_rotationAngle = 90f;
    public Transform anchorPoint;

    private Quaternion initialRotation;
    private bool isRotating = false;
    private float elapsedTime = 0;


    private void OnEnable()
    {
        GetComponent<TapGesture>().Tapped += tappedHandler;
        EventManager.StartListening(EventNames.ResetARGame, ResetTiles);
    }

    private void OnDisable()
    {
        GetComponent<TapGesture>().Tapped -= tappedHandler;
        EventManager.StopListening(EventNames.ResetARGame, ResetTiles);
    }

    private void Start()
    {
        initialRotation = anchorPoint.localRotation;
    }

    private void ResetTiles()
    {
        anchorPoint.localRotation = initialRotation;
    }

    private void tappedHandler(object sender, EventArgs e)
    {
        if (isRotating == false)
        {
            elapsedTime = 0;
            StartCoroutine(RotateTileCoroutine(anchorPoint.localRotation));
        }
    }

    IEnumerator RotateTileCoroutine(Quaternion originalRotation)
    {
        Quaternion targetRotation = originalRotation;
        targetRotation *= Quaternion.Euler(0, 0, m_rotationAngle);
        while (elapsedTime < m_rotationTime)
        {
            elapsedTime += Time.deltaTime;

            isRotating = true;

            anchorPoint.localRotation = Quaternion.Slerp(originalRotation, targetRotation, elapsedTime / m_rotationTime);
            yield return new WaitForEndOfFrame();
        }
        isRotating = false;

    }
}
