using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using TouchScript.Hit;
public class AutoRotateTile : MonoBehaviour
{
    public float m_rotationOccurence = 1f;
    public float m_rotationTime = 0.5f;
    public float m_rotationAngle = 90f;

    private Quaternion initialRotation;
    private bool isRotating = false;
    private float elapsedTime = 0; 
    private float elapsedTimeOccurence = 0;


    private void OnEnable()
    {
        //GetComponent<TapGesture>().Tapped += tappedHandler;
        EventManager.StartListening(EventNames.ResetARGame, ResetTiles);
    }

    private void OnDisable()
    {
        //GetComponent<TapGesture>().Tapped -= tappedHandler;
        EventManager.StopListening(EventNames.ResetARGame, ResetTiles);
    }

    private void Start()
    {
        initialRotation = this.transform.rotation;
    }

    private void Update()
    {
        if (elapsedTimeOccurence > m_rotationOccurence)
        {
            elapsedTimeOccurence = 0;
            elapsedTime = 0;
            Debug.Log("occurence");
            StartCoroutine(RotateTileCoroutine(this.transform.rotation));
        }
        if (isRotating == false)
            elapsedTimeOccurence += Time.deltaTime;
    }

    private void ResetTiles()
    {
        this.transform.rotation = initialRotation;
    }

    IEnumerator RotateTileCoroutine(Quaternion originalRotation)
    {
        Quaternion targetRotation = originalRotation;
        targetRotation *= Quaternion.Euler(0, 0, m_rotationAngle);
        while (elapsedTime < m_rotationTime)
        {
            elapsedTime += Time.deltaTime;
            isRotating = true;

            this.transform.rotation = Quaternion.Slerp(originalRotation, targetRotation, elapsedTime / m_rotationTime);
            yield return new WaitForEndOfFrame();
        }
        isRotating = false;
    }
}
