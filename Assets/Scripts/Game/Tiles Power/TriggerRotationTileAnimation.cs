using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using TouchScript.Hit;


public class TriggerRotationTileAnimation : MonoBehaviour
{
    public Animator m_animator;
    private bool Open = false;
    int animationCounter = 0;


    private void OnEnable()
    {
        GetComponent<TapGesture>().Tapped += tappedHandler;
        EventManager.StartListening(EventNames.ResetARGame, ResetTiles);
        EventManager.StartListening(EventNames.ResetAll, ResetTiles);
        animationCounter = 0;
    }

    private void OnDisable()
    {
        GetComponent<TapGesture>().Tapped -= tappedHandler;
        EventManager.StopListening(EventNames.ResetARGame, ResetTiles);
        EventManager.StopListening(EventNames.ResetAll, ResetTiles);
        m_animator.Rebind();
        animationCounter = 0;
    }

    private void Start()
    {

    }

    private void ResetTiles()
    {
        m_animator.Rebind();
        animationCounter = 0;
    }

    private void tappedHandler(object sender, EventArgs e)
    {

        if (animationCounter % 4 == 0)
        {     
            m_animator.SetTrigger("0to90");
        }
        if (animationCounter % 4 == 1)
        {
            m_animator.SetTrigger("90to180");
        }
        if (animationCounter % 4 == 2)
        {
            m_animator.SetTrigger("180to270");
        }
        if (animationCounter % 4 == 3)
        {
            m_animator.SetTrigger("270to0");
        }
        animationCounter++;
    }

}
