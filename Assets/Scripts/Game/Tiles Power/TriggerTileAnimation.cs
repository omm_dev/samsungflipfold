using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using TouchScript.Hit;


public class TriggerTileAnimation : MonoBehaviour
{
    public Animator m_animator;
    private bool Open = false;


    private void OnEnable()
    {
        GetComponent<TapGesture>().Tapped += tappedHandler;
        EventManager.StartListening(EventNames.ResetARGame, ResetTiles);
    }

    private void OnDisable()
    {
        GetComponent<TapGesture>().Tapped -= tappedHandler;
        EventManager.StopListening(EventNames.ResetARGame, ResetTiles);
    }

    private void Start()
    {
       
    }

    private void ResetTiles()
    {
        m_animator.Rebind();
    }

    private void tappedHandler(object sender, EventArgs e)
    {

        if (m_animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
        {
            Open = !Open;

            m_animator.SetBool("Open", Open);
        }
    }

   
}
