using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BallSpeed : MonoBehaviour
{

    public float m_constantSpeed = 1.0f;
    public float m_smoothingFactor = 1.0f;

    public Rigidbody ballRigidbody;
    public Vector3 initialBallPosition;

    private bool constantVelocity = true;
    private bool hasNudge = false;



    // Start is called before the first frame update
    void Start()
    {
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventNames.ResetARGame, ResetBall);
        EventManager.StartListening(EventNames.ResetAll, ResetAll);
        EventManager.StartListening(EventNames.StartGame, StartGame);
        EventManager.StartListening(EventNames.EndGame, EndGame);
        EventManager.StartListening(EventNames.Nudge, Nudge);
       ResetAll();
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventNames.ResetARGame, ResetBall);
        EventManager.StopListening(EventNames.ResetAll, ResetAll);
        EventManager.StopListening(EventNames.StartGame, StartGame);
        EventManager.StopListening(EventNames.EndGame, EndGame);
        EventManager.StopListening(EventNames.Nudge, Nudge);

    }


    private void ResetBall()
    {
        this.transform.localPosition = initialBallPosition;
        ballRigidbody.isKinematic = false;
        constantVelocity = true;
    }

    private void ResetAll()
    {
        this.transform.localPosition = initialBallPosition;
        ballRigidbody.isKinematic = true;
        constantVelocity = false;
    }

    private void StartGame()
    {
        ballRigidbody.isKinematic = false;
        Debug.Log("start Game");
        constantVelocity = true;
    }

    private void EndGame()
    {
        constantVelocity = false;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (constantVelocity == true && hasNudge == false)
        {

            var cvel = ballRigidbody.velocity;
            var tvel = cvel.normalized * m_constantSpeed;
            ballRigidbody.velocity = Vector3.Lerp(cvel, tvel, Time.deltaTime * m_smoothingFactor);
        }
        if (hasNudge == true)
        {

            var cvel = ballRigidbody.velocity;
            var tvel = cvel.normalized * m_constantSpeed;
            Debug.Log(Vector3.Distance(ballRigidbody.velocity, tvel));
            if (Vector3.Distance(ballRigidbody.velocity, tvel) <= m_constantSpeed)
            {

                hasNudge = false;
            }

        }
    }

    public void Nudge()
    {
        Debug.Log("Nudge");
        hasNudge = true;

        ballRigidbody.AddForce(new Vector3(Random.Range(-1f, 1f),0,1) * m_constantSpeed  * 10,ForceMode.Impulse);
    }
}
