using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEndGame : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ball")
        {
            other.GetComponent<Rigidbody>().isKinematic = true;
            EventManager.TriggerEvent(EventNames.EndGame);
            Debug.Log("Game has Ended");
        }
    }
}
