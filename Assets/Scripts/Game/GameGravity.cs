using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameGravity : MonoBehaviour
{
    public enum ImageTargetType {Bottom_Left, Top_Left, Central, Bottom_right , Top_right, Nothing }
    public ImageTargetType imageTarget = ImageTargetType.Nothing;

    public static int invertGravity = -1;
    public static ImageTargetType currentTargetType = ImageTargetType.Nothing;
    // Update is called once per frame
    void Update()
    {
        if (imageTarget == currentTargetType)
        {
            Physics.gravity = invertGravity * transform.forward * 9.81f;
#if UNITY_EDITOR
            Physics.gravity = invertGravity * transform.forward * 9.81f;
#endif
        }
    }

    public void SetMainImage(int i)
    {
        currentTargetType = (ImageTargetType)i;
    }
}
