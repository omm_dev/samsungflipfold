using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IDropHandler
{
	public bool isDouble = false;
	public GameObject item
	{
		get
		{
			if (transform.childCount > 0)
			{
				return transform.GetChild(0).gameObject;
			}
			return null;
		}
	}

	#region IDropHandler implementation
	public void OnDrop(PointerEventData eventData)
	{
		if (HandleDoubleTiles() == false)
		{
			return;
		}
		if (transform.childCount > 0)
		{
			if (transform.GetChild(0).GetComponent<TileInformation>().m_isDoubleTile == true)
			{
				HandleDeletingDoubleTile();
			}
			Destroy(transform.GetChild(0).gameObject);
		}
		DragHandeler.itemBeingDragged.transform.SetParent(transform);
		DragHandeler.itemBeingDragged.GetComponent<Image>().raycastTarget = false;
		ExecuteEvents.ExecuteHierarchy<IHasChanged>(gameObject, null, (x, y) => x.HasChanged());
	}


	private void HandleDeletingDoubleTile()
    {
		Transform nextTile = GetNext().GetChild(0) ;
		Transform prevTile = GetPrev().GetChild(0);
		TileInformation nextTileInformation = nextTile.GetComponent<TileInformation>();
		TileInformation prevTileInformation = prevTile.GetComponent<TileInformation>();
		if (nextTile != null && nextTileInformation.m_isDoubleTile == true && nextTileInformation.m_isVisible == false)
		{
			GameObject obj = Instantiate(DragHandeler.itemBeingDragged, nextTile);
			Destroy(nextTile.GetChild(0).gameObject);
		}
		if (prevTile != null && prevTileInformation.m_isDoubleTile == true && prevTileInformation.m_isVisible == false)
		{
			GameObject obj = Instantiate(DragHandeler.itemBeingDragged, prevTile);
			Destroy(prevTile.GetChild(0).gameObject);
		}
	}


	private bool HandleDoubleTiles()
	{
		if (DragHandeler.itemBeingDragged.GetComponent<TileInformation>().m_isDoubleTile == true)
		{
		
			Transform nextTile = GetNext().GetChild(0);
			Transform prevTile = GetPrev().GetChild(0);
			Debug.Log(nextTile.name);
			
			TileInformation nextTileInformation = nextTile.GetComponent<TileInformation>();
			TileInformation prevTileInformation = prevTile.GetComponent<TileInformation>();
			if (nextTile == null)
				return false;

			if (transform.GetChild(0).GetComponent<TileInformation>().m_isDoubleTile == true)
				return false;

			if (nextTileInformation && nextTileInformation.m_isDoubleTile == true)
				return false;

			if (nextTile.position.y != transform.position.y)
				return false;

			if (nextTile)
			{
				Destroy(nextTile.gameObject);
				GameObject obj = Instantiate(DragHandeler.itemBeingDragged, nextTile.parent);
				obj.GetComponent<Image>().raycastTarget = false;
				obj.GetComponent<Image>().enabled = false;
				obj.GetComponent<TileInformation>().m_isVisible = false;
				//ExecuteEvents.ExecuteHierarchy<IHasChanged>(obj, null, (x, y) => x.HasChanged());
				return true;
			}

		}
		return true;
	}


	Transform GetNext()
	{
		var myself = transform;
		var parent = transform.parent;
		var childCount = parent.childCount;
		for (int i = 0; i < childCount - 1; i++)
		{ // skip the last, as it doesn't have a successor
			if (parent.GetChild(i) == myself)
				return parent.GetChild(i + 1);
		}
		return null;
	}

	Transform GetPrev()
	{
		var myself = transform;
		var parent = transform.parent;
		var childCount = parent.childCount;
		for (int i = 1; i < childCount; i++)
		{ // skip the last, as it doesn't have a successor
			if (parent.GetChild(i) == myself)
				return parent.GetChild(i - 1);
		}
		return null;
	}
	#endregion
}
