using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


public class DragHandeler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	public static GameObject itemBeingDragged;
	Vector3 startPosition;
	Transform startParent;

	#region IBeginDragHandler implementation

	public void OnBeginDrag(PointerEventData eventData)
	{
		itemBeingDragged = Instantiate(this.gameObject, startPosition, Quaternion.identity, GetComponentInParent<Canvas>().transform);
		RectTransform copyRect = this.GetComponent<RectTransform>();
		CopyRectTransform(itemBeingDragged.GetComponent<RectTransform>(), copyRect);



		startParent = transform.parent;
		itemBeingDragged.GetComponent<CanvasGroup>().blocksRaycasts = false;

	}

	#endregion

	#region IDragHandler implementation

	public void OnDrag(PointerEventData eventData)
	{
		itemBeingDragged.transform.position = eventData.position;
	}

	#endregion

	#region IEndDragHandler implementation

	public void OnEndDrag(PointerEventData eventData)
	{

		itemBeingDragged.GetComponent<CanvasGroup>().blocksRaycasts = false;
		if (itemBeingDragged.transform.parent == GetComponentInParent<Canvas>().transform)
		{
			Destroy(itemBeingDragged);
		}
		else
			itemBeingDragged = null;
	}


	private void CopyRectTransform(RectTransform Rect, RectTransform copyRect)
	{


		Rect.anchorMin = copyRect.anchorMin;
		Rect.anchorMax = copyRect.anchorMax;
		Rect.anchoredPosition = copyRect.anchoredPosition;
		Rect.sizeDelta = copyRect.sizeDelta;
	}

	#endregion



}
