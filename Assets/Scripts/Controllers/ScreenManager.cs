﻿using Pixelplacement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenManager : MonoBehaviour
{
    //public static TKTapRecognizer TKTapRecognizer;
    //public static TKSwipeRecognizer TKSwipeRecognizer;
    public Color m_FlipColor = new Color(191, 177, 210, 1);
    public Color m_FoldColor = new Color(202, 198, 196, 1);
    public GameObject m_IdleScreen;
    public GameObject m_MazeInfo;
    public GameObject m_TileDemoScreen;
    public GameObject m_HelpMode;
    public GameObject m_GameUi;
    public GameObject m_Congrats;
    public GameObject m_VideoPage;
    public GameObject m_SignUp;
    public GameObject m_Rank;


    private static bool m_isAnimating;
    public AnimationCurve m_animationCurve;

    public static bool IsAnimating
    {
        get
        {
            return m_isAnimating;
        }
    }

    private void Next(AppState current, AppState next)
    {
        if (m_isAnimating)
        {
            return;
        }

        m_isAnimating = true;

        AnimatedScreen currentScreen = FindScreen(current);
        AnimatedScreen nextScreen = FindScreen(next);

        EnterTransition(currentScreen, nextScreen, next);
    }

    private void Back(AppState current, AppState next)
    {
        if (m_isAnimating)
        {
            return;
        }

        m_isAnimating = true;

        AnimatedScreen currentScreen = FindScreen(current);
        AnimatedScreen nextScreen = FindScreen(next);

        EnterTransitionBack(currentScreen, nextScreen, next);
    }

    public AnimatedScreen FindScreen(AppState screenType)
    {
        switch (screenType)
        {
            case AppState.Idle:
                {
                    return m_IdleScreen.GetComponent<AnimatedScreen>();
                }
            case AppState.MazeInfo:
                {
                    return m_MazeInfo.GetComponent<AnimatedScreen>();
                }
            case AppState.TileDemo:
                {
                    return m_TileDemoScreen.GetComponent<AnimatedScreen>();
                }
            case AppState.HelpMode:
                {
                    return m_HelpMode.GetComponent<AnimatedScreen>();
                }
            case AppState.ARgame:
                {
                    return m_GameUi.GetComponent<AnimatedScreen>();
                }
            case AppState.Congrats:
                {
                    return m_Congrats.GetComponent<AnimatedScreen>();
                }
            case AppState.Video:
                {
                    return m_VideoPage.GetComponent<AnimatedScreen>();
                }
            case AppState.SignUp:
                {
                    return m_SignUp.GetComponent<AnimatedScreen>();
                }
            case AppState.Rank:
                {
                    return m_Rank.GetComponent<AnimatedScreen>();
                }

            default: return null;
        }
    }

    public void Enter(AnimatedScreen target)
    {
        var width = target.GetComponent<RectTransform>().rect.width;
        float offset = width;

        Tween.Value(offset, 0, (value) =>
        {
            target.GetComponent<RectTransform>().offsetMin = new Vector2(value -1, -1);
            target.GetComponent<RectTransform>().offsetMax = new Vector2(value -1, -1);
        }, 0.3f, 0);
    }

    public void EnterTransition(AnimatedScreen current, AnimatedScreen target, AppState targetType)
    {
        var width = target.GetComponent<RectTransform>().rect.width;
        float offset = width;

        if (current == null)
        {
            Enter(target);
            return;
        }

        Tween.Value(offset, 0, (value) =>
        {
            target.GetComponent<RectTransform>().offsetMin = new Vector2(value -1, -1);
            target.GetComponent<RectTransform>().offsetMax = new Vector2(value + 1, +1);
            current.GetComponent<RectTransform>().offsetMin = new Vector2(-width + value, 0);
            current.GetComponent<RectTransform>().offsetMax = new Vector2(-width + value, 0);
        }, 0.5f, 0, easeCurve: m_animationCurve, startCallback: () => { target.OnEnterStarted(); AppEvents.OnScreenTransitionStarted?.Invoke(targetType); }, completeCallback: () => { target.OnEnterEnded(); current.OnExitEnded(); AppEvents.OnScreenTransitionEnded?.Invoke(targetType); m_isAnimating = false; });
    }

    public void EnterTransitionBack(AnimatedScreen current, AnimatedScreen previous, AppState targetType)
    {
        var width = previous.GetComponent<RectTransform>().rect.width;
        float offset = width;

        if (current == null)
        {
            EnterFromLeft(previous);
            return;
        }

        Tween.Value(-offset, 0, (value) =>
        {
            previous.GetComponent<RectTransform>().offsetMin = new Vector2(value -1, -1);
            previous.GetComponent<RectTransform>().offsetMax = new Vector2(value -1, -1);
            current.GetComponent<RectTransform>().offsetMin = new Vector2(width + value, 0);
            current.GetComponent<RectTransform>().offsetMax = new Vector2(width + value, 0);
        }, 0.5f, 0, easeCurve: m_animationCurve, startCallback: () => { previous.OnEnterStarted(); AppEvents.OnScreenTransitionStarted?.Invoke(targetType); }, completeCallback: () => { previous.OnEnterEnded(); AppEvents.OnScreenTransitionEnded?.Invoke(targetType); current.OnExitEnded(); m_isAnimating = false; });
    }

    public void EnterFromLeft(AnimatedScreen target)
    {
        var width = target.GetComponent<RectTransform>().rect.width;
        float offset = width;

        Tween.Value(-offset, 0, (value) =>
        {
            target.GetComponent<RectTransform>().offsetMin = new Vector2(value -1, 0);
            target.GetComponent<RectTransform>().offsetMax = new Vector2(value -1, 0);
        }, 0.3f, 0);
    }

    public void Exit(GameObject target)
    {
        var width = target.GetComponent<RectTransform>().rect.width;
        float offset = 0;

        Tween.Value(offset, width, (value) =>
        {
            target.GetComponent<RectTransform>().offsetMin = new Vector2(-value -1, 0);
            target.GetComponent<RectTransform>().offsetMax = new Vector2(-value -1, 0);
        }, 0.3f, 0);

    }

    public void ExitToRight(GameObject target)
    {
        var width = target.GetComponent<RectTransform>().rect.width;
        float offset = 0;

        Tween.Value(offset, -width, (value) =>
        {
            target.GetComponent<RectTransform>().offsetMin = new Vector2(-value -1, 0);
            target.GetComponent<RectTransform>().offsetMax = new Vector2(-value -1, 0);
        }, 0.3f, 0);
    }

    //private void CreateTapGesture()
    //{
    //	TKTapRecognizer = new TKTapRecognizer();
    //	var x = 0;
    //	var y = 0;
    //	var width = Screen.width;
    //	var height = Screen.height;

    //	TKTapRecognizer.boundaryFrame = new TKRect(x, y, width, height);

    //	TouchKit.addGestureRecognizer(TKTapRecognizer);
    //}

    //private void CreateBackSwipeGesture()
    //{
    //	TKSwipeRecognizer = new TKSwipeRecognizer();
    //	var x = 0;
    //	var y = 0;
    //	var width = Screen.width;
    //	var height = Screen.height;

    //	TKSwipeRecognizer.boundaryFrame = new TKRect(x, y, width, height);

    //	TouchKit.addGestureRecognizer(TKSwipeRecognizer);
    //}

    private void Awake()
    {
        AppEvents.NextRequested += Next;
        AppEvents.BackRequested += Back;

        //CreateTapGesture();
        //CreateBackSwipeGesture();

        m_IdleScreen.SetActive(true);
        m_MazeInfo.SetActive(true);
        m_TileDemoScreen.SetActive(true);
        m_HelpMode.SetActive(true);
        m_GameUi.SetActive(true);
        m_Congrats.SetActive(true);
        m_VideoPage.SetActive(true);
        m_SignUp.SetActive(true);
        m_Rank.SetActive(true);
    }
}