using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetBtnColorScript : MonoBehaviour
{
    public Image m_ButtonUI;
    // Start is called before the first frame update
    void Start()
    {
        ScreenManager m_screenManager = FindObjectOfType<ScreenManager>();
        if (Screen.width == 1080)
        {
            m_ButtonUI.color = m_screenManager.m_FlipColor;
        }
        else
        {
            m_ButtonUI.color = m_screenManager.m_FoldColor;
        }
    }

}
