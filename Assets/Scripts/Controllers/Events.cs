﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class AppEvents
{
    public static UnityAction<AppState, AppState> BackRequested;
    public static UnityAction<AppState, AppState> NextRequested;
    public static UnityAction<AppState> OnScreenTransitionStarted;
    public static UnityAction<AppState> OnScreenTransitionEnded;
    public static UnityAction ExperienceEnded;
    public static UnityAction Reset;
    public static UnityAction CompositeFinished;
    public static UnityAction<string> LanguageSwitched;
    public static UnityAction TriggerDownloadTranslations;
}