﻿using UnityEngine;
using Stateless;
using Assets.Scripts.Models.DataStore;
using System.Collections.Generic;
using System;

public enum AppState
{
    Idle = 0,
    MazeInfo = 1,
    TileDemo = 2,
    HelpMode = 3,
    ARgame = 4,
    Congrats = 5,
    Video = 6,
    SignUp = 7,
    Rank = 8
}

// Enums for Navigating
// If these change it may break transitions set in the editor.
public enum AppTrigger
{
    // General
    Next = 0,
    SkipNext = 1,
    Back = 2,
    Exit = 3,
    Reset = 4

}

public class AppController : MonoSingleton<MonoBehaviour>
{
    private AppState _currentState = AppState.Idle;
    private AppState _prevState;

    public StateMachine<AppState, AppTrigger> Machine { get; }
    public AppState CurrentState { get { return _currentState; } }





    public AppController()
    {
        Machine = new StateMachine<AppState, AppTrigger>(() => _currentState, s => { _prevState = _currentState; _currentState = s; });

        Machine.Configure(AppState.Idle)
        .OnEntry((t) => { HandleTransition(t); })
        .OnExit(() => GenerateNewUserId())
       .PermitDynamic(AppTrigger.Back, GetLastScreen)
       .Permit(AppTrigger.Next, AppState.MazeInfo);

        Machine.Configure(AppState.MazeInfo)
        .OnEntry((t) => { HandleTransition(t); })
       .Permit(AppTrigger.Back, AppState.Idle)
       .Permit(AppTrigger.Next, AppState.TileDemo);

        Machine.Configure(AppState.TileDemo)
        .OnEntry((t) => { HandleTransition(t); })
        .OnExit(() => GenerateNewUserId())
       .PermitDynamic(AppTrigger.Back, GetLastScreen)
       .Permit(AppTrigger.Next, AppState.HelpMode);

        Machine.Configure(AppState.HelpMode)
        .OnEntry((t) => { HandleTransition(t); })
       .Permit(AppTrigger.Back, AppState.TileDemo)
       .Permit(AppTrigger.Next, AppState.ARgame);

        Machine.Configure(AppState.ARgame)
       .OnEntry((t) => { HandleTransition(t); })
      .Permit(AppTrigger.Back, AppState.Idle)
      .Permit(AppTrigger.Reset, AppState.Idle)
      .Permit(AppTrigger.Next, AppState.Congrats);

        Machine.Configure(AppState.Congrats)
       .OnEntry((t) => { HandleTransition(t); })
      .Permit(AppTrigger.Next, AppState.Video)
      .Permit(AppTrigger.Reset, AppState.Idle);

        Machine.Configure(AppState.Video)
        .OnEntry((t) => { HandleTransition(t); })
       .PermitDynamic(AppTrigger.Back, GetLastScreen)
       .Permit(AppTrigger.Next, AppState.SignUp)
       .Permit(AppTrigger.Reset, AppState.Idle);

        Machine.Configure(AppState.SignUp)
        .OnEntry((t) => { HandleTransition(t); })
       .PermitDynamic(AppTrigger.Back, GetLastScreen)
       .Permit(AppTrigger.Reset, AppState.Idle)
       .Permit(AppTrigger.Next, AppState.Rank);

        Machine.Configure(AppState.Rank)
        .OnEntry((t) => { HandleTransition(t); })
       .PermitDynamic(AppTrigger.Back, GetLastScreen)
       .Permit(AppTrigger.Reset, AppState.Idle)
       .Permit(AppTrigger.Next, AppState.Idle);




    }

    private void GenerateNewUserId()
    {
        InMemoryDataStore.Instance.UserId = Guid.NewGuid().ToString();
    }




    private AppState ExitApp()
    {
        return AppState.Idle;
    }



    private AppState GetLastScreen()
    {
        return _prevState;
    }

    private void HandleTransition(StateMachine<AppState, AppTrigger>.Transition transition)
    {
        if (transition.Trigger == AppTrigger.Back)
        {
            AppEvents.BackRequested(transition.Source, transition.Destination);
        }
        else
        {
            AppEvents.NextRequested(transition.Source, transition.Destination);
        }
    }

    public void Back()
    {
        Machine.Fire(AppTrigger.Back);
    }

    public void Next()
    {
        if (ScreenManager.IsAnimating) return;

        Machine.Fire(AppTrigger.Next);
    }
    public void ResetApp()
    {
        EventManager.TriggerEvent(EventNames.ResetARGame);
        EventManager.TriggerEvent(EventNames.ResetAll);
        Machine.Fire(AppTrigger.Reset);
    }

    public void SkipNext()
    {
        Machine.Fire(AppTrigger.SkipNext);
    }

    public void FireTrigger(AppTrigger trigger)
    {
        Machine.Fire(trigger);
    }

    public void FireTrigger(int trigger)
    {
        Machine.Fire((AppTrigger)trigger);
    }

}

