﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SettingsManager : MonoSingleton<SettingsManager>
{
    public static Action SettingsLoaded;
    Dictionary<string, string> Settings;

    public SettingsManager()
    {
        Instance = this;
    }
    
    void Start()
    {
        LoadSettings();
    }


    private void LoadSettings()
    {
        string settingspath = Application.streamingAssetsPath + "/settings.config";

        Dictionary<string, string> tempSettings = new Dictionary<string, string>();

        if (File.Exists(settingspath))
        {

            using (StreamReader reader = new StreamReader(settingspath))
            {

                while (!reader.EndOfStream)
                {
                    string[] line = reader.ReadLine().Split('=');
                    tempSettings.Add(line[0], line[1]);
                }

                reader.Close();

            }
            
            Settings = tempSettings;
            SettingsLoaded?.Invoke();

        }
        else
        {
            Debug.LogError("Cannot Find Settings File at: "+ settingspath);
        }

    }

    public bool SettingsContainsKey(string _key)
    {
        if (Settings.ContainsKey(_key))
        {
            return true;
        }
        else
        {
            Debug.LogError("Settings does not contain a key of: "+_key);
            return false;
        }
    }

    public string GetSettingsValue(string _key)
    {
        if (SettingsContainsKey(_key))
        {
            return Settings[_key];
        }
        else
        {
            return null;
        }
    }
}
