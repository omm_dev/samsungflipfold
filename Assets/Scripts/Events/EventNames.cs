using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventNames
{

    public const string ResetARGame = "ResetARGame";
    public const string EndGame = "EndGame";
    public const string StartGame = "StartGame";
    public const string Nudge = "Nudge";
    public const string ResetAll = "ResetAll";


}
