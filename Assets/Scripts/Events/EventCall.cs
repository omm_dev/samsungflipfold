using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventCall : MonoBehaviour
{
    public string m_eventName;

    public void CallEvent()
    {
        if (EventManager.instance != null)
            EventManager.TriggerEvent(m_eventName);
    }    
}
