using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisablePlaneGround : MonoBehaviour
{
    bool isObjectFound = false;
    
    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        if (isObjectFound == false)
        {
            GameObject obj = GameObject.Find("emulator_ground_plane");
            if (obj != null)
            {
                obj.SetActive(false);
                isObjectFound = true;
            }
        }
        
    }
}
