using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GridManager : MonoBehaviour
{
    public GameObject m_prefabSlot;
    public Transform m_gridTransform;
    

 
    // Default Tiles that you can drag and drop, situated at the bottom of the scene canvas
    //private List<TileInformation> originalTiles;
    public List<TileInformation> m_originalTiles;
    private Dictionary<char, GameObject> originalTilesDictionary;


    private string filePath = Application.streamingAssetsPath + "/GridEditorTilesLayout.txt";


    // List of a list, so a grid of character numbers (0 to 6)
    private List<List<char>> characterGrid;

    private List<TileInformation> tiles;


    void Start()
    {
        LoadOriginalTiles();
        LoadGrid();
    }

    void LoadOriginalTiles()
    {
        originalTilesDictionary = new Dictionary<char, GameObject>();
        for (int i = 0; i < m_originalTiles.Count; i++)
        {
            originalTilesDictionary.Add(m_originalTiles[i].m_number, m_originalTiles[i].m_tilePrefab);
        }
    }

    void LoadGrid()
    {
        string text = "";
        if (Application.platform == RuntimePlatform.Android)
           text = ReadTextFileAndroid(filePath);
        else
           text = ReadTextFile(filePath);

        GetTilesFromTextFile(text);
        InstantiateSlots();
        ImplementTiles();
    }

    void GetTilesFromTextFile(string text)
    {
            int countLines = 0;
            characterGrid = new List<List<char>>();
            characterGrid.Add(new List<char>());
            for (int c = 0; c < text.Length; c++)
            {
                if (text[c] == '\n')
                {
                    characterGrid.Add(new List<char>());
                    countLines++;
                }
                else if (System.Char.IsDigit(text[c]) == true)
                {
                    characterGrid[countLines].Add(text[c]);
                }
            }
        }

    string ReadTextFileAndroid(string file_path)
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            UnityWebRequest www = UnityEngine.Networking.UnityWebRequest.Get(file_path);
            www.SendWebRequest();
            while (!www.downloadHandler.isDone)
            {
            }
            return www.downloadHandler.text;

        }
        return "";
    }

    string ReadTextFile(string file_path)
    {
        StreamReader inp_stm = new StreamReader(file_path);
        string text = inp_stm.ReadToEnd();
        inp_stm.Close();
        return text;
    }


    public void WriteTextFile()
    {
        StreamWriter writer = new StreamWriter(filePath, false);
        GetTilesFromSlots();
        for (int y = 0; y < characterGrid.Count; y++)
        {
            for (int x = 0; x < characterGrid[y].Count; x++)
            {
                writer.Write(tiles[y * (characterGrid.Count) + x].m_number);
            }
            if (y + 1 < characterGrid.Count)
                writer.Write('\n');
        }
        writer.Close();
    }

    //save To file
    void GetTilesFromSlots()
    {
        for (int i = 0; i < m_gridTransform.childCount; i++)
        {
            tiles[i] = m_gridTransform.GetChild(i).GetChild(0).GetComponent<TileInformation>();
        }

    }

    void ImplementTiles()
    {
        tiles = new List<TileInformation>();
        for (int y = 0; y < characterGrid.Count; ++y)
        {
            for (int x = 0; x < characterGrid[y].Count; ++x)
            {
                GameObject instantiatedTile = Instantiate(originalTilesDictionary[characterGrid[y][x]], m_gridTransform.GetChild(y * (characterGrid.Count) + x));
                tiles.Add(instantiatedTile.GetComponent<TileInformation>());
                if (tiles[tiles.Count -1].m_isDoubleTile == true)
                {
                    instantiatedTile = Instantiate(originalTilesDictionary[characterGrid[y][x]], m_gridTransform.GetChild(y * (characterGrid.Count) + x + 1));
                    tiles.Add(instantiatedTile.GetComponent<TileInformation>());
                    if (instantiatedTile.GetComponent<Image>())
                    {
                        instantiatedTile.GetComponent<Image>().enabled = false;
                        tiles[tiles.Count - 1].m_isVisible = false;
                    }
                    else
                        instantiatedTile.SetActive(false);
                    ++x;
                }

            }
        }
    }

    void InstantiateSlots()
    {
        for (int y = 0; y < characterGrid.Count; ++y)
        {
            for (int x = 0; x < characterGrid[y].Count; ++x)
            {
                Instantiate(m_prefabSlot, this.m_gridTransform);
            }
        }
    }
}
