using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleImageTargets : MonoBehaviour
{
    public GameObject m_grid;
    public Vector3 m_position;
    private void OnEnable()
    {
        EventManager.StartListening(EventNames.ResetAll, ResetAll);
       m_grid.transform.parent = this.transform.parent;


    }

    private void OnDisable()
    {
        EventManager.StopListening(EventNames.ResetAll, ResetAll);

        m_grid.SetActive(false);

    }

    public void SetAsParent()
    {
        m_grid.SetActive(true);
        m_grid.transform.parent = this.transform;
        m_grid.transform.localPosition = m_position;
        m_grid.transform.localEulerAngles = new Vector3(90, 0, 0);
    }

    public void ResetAll()
    {
        m_grid.SetActive(false);

    }
}
