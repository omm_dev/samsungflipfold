using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TileInformation : MonoBehaviour
{
    public char m_number;
    public GameObject m_tilePrefab;
    public bool m_isDoubleTile = false;
    public bool m_isEditor = false;

    [HideInInspector]
    public bool m_isVisible = true;

    public TileInformation(char _number, GameObject _gameObject)
    {
        m_number = _number;
        m_tilePrefab = _gameObject;
    }

}
