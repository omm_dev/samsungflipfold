﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Screen_Start : AnimatedScreen
{

    public TextMeshProUGUI[] m_TextColoured;
    public TextMeshProUGUI m_BodyText;
    public TextMeshProUGUI m_TitleText;
    [Space(10)]
    public String m_titleStart;
    public String m_titleEnd;
    [Space(10)]
    public String m_BodyFillStart;
    public String m_BodyFillEnd;
    [Space(10)]
    public GameObject m_FoldCoverImage;
    public GameObject m_FLipCoverImage;


    private void Start()
    {
        //SETS CHOSEN COLOR
        Color CurrentPhoneColorScheme;
        ScreenManager m_screenManager = FindObjectOfType<ScreenManager>();
        if (Screen.width == 1080)
        {
            CurrentPhoneColorScheme = m_screenManager.m_FlipColor;
            m_TitleText.text = m_titleStart+ "Flip3"+ m_titleEnd;
            m_BodyText.text = m_BodyFillStart + "Flip3" + m_BodyFillEnd;
            m_FLipCoverImage.SetActive(true);
            m_FoldCoverImage.SetActive(false);
        }
        else
        {
            CurrentPhoneColorScheme = m_screenManager.m_FoldColor;
            m_TitleText.lineSpacing = 0f;
            m_TitleText.text = m_titleStart + "Fold3" + m_titleEnd;
            m_BodyText.text = m_BodyFillStart + "Fold3" + m_BodyFillEnd;
            m_FLipCoverImage.SetActive(false);
            m_FoldCoverImage.SetActive(true);

        }
        //CHANGE AR MAZE TEXT TO DIFFERENT COLOR
        //String BodyFill = m_BodyFillStart + ColorUtility.ToHtmlStringRGBA(CurrentPhoneColorScheme) + m_BodyFillEnd;
        //m_BodyText.text = BodyFill;
        //SETS TEXT COLOURS ON THIS SCREEN
        if (m_TextColoured.Length != 0)
        {
            foreach (TextMeshProUGUI text in m_TextColoured)
            {
                if (Screen.width == 1080)
                {
                    text.color = CurrentPhoneColorScheme;
                }
                else
                {
                    text.color = CurrentPhoneColorScheme;
                }
            }
        }

    }
    public override void OnEnterStarted()
    {
        base.OnEnterEnded();
        EventManager.TriggerEvent(EventNames.ResetAll);
    }

    public override void OnExitStarted()
    {
    }
}

