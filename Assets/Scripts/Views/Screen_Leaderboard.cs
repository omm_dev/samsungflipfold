using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Screen_Leaderboard : AnimatedScreen 
{
    public GameObject overlay;
    public Component_Leaderboard componentLeaderboard;

    private int arrayPosition;

    IEnumerator Timer(float waitTime, Action callback)
    {
        yield return new WaitForSeconds(waitTime);
        callback();
    }

    public void Setup(RankInfo[] rankInfo, int _arrayPosition)
    {
        if (componentLeaderboard == null) return;
        componentLeaderboard.Setup(rankInfo);
        arrayPosition = _arrayPosition;
    }

    void StartAnimation()
    {
        if (overlay == null) return;
        if (componentLeaderboard == null) return;
        overlay.SetActive(false);
        componentLeaderboard.StartAnimation(arrayPosition);
    }

    public override void OnEnterEnded()
    {
        base.OnEnterEnded();
        StartCoroutine(Timer(5.0f, StartAnimation));
    }

   

}
