﻿using Pixelplacement;
using System.Collections;
using System.Collections.Generic;
using UnityApplicationInsights;
using UnityEngine;

public class AnimatedScreen : MonoBehaviour
{
    public bool m_isActive;
    private float m_currentWidth;
   
    public virtual void ResetPosition()
    {
        var width = GetComponent<RectTransform>().rect.width;
        gameObject.GetComponent<RectTransform>().offsetMin = new Vector2(width, 0);
        gameObject.GetComponent<RectTransform>().offsetMax = new Vector2(width, 0);
    }

    public virtual void OnEnterEnded()
    {
        m_isActive = true;
        //ScreenManager.TKSwipeRecognizer.gestureRecognizedEvent += SwipeGestureCallback;
        LogPageView();
    }

    public virtual void OnEnterStarted()
    {
        
    }

    public virtual void OnExitStarted()
    {

    }

    public virtual void OnExitEnded()
    {
        m_isActive = false;
        //ScreenManager.TKSwipeRecognizer.gestureRecognizedEvent -= SwipeGestureCallback;
    }

    public virtual void Update()
    {
        var currentWidth = GetComponent<RectTransform>().rect.width;
        if (GetComponent<RectTransform>().rect.width != m_currentWidth)
        {
            m_currentWidth = currentWidth;
            if(!m_isActive)
            {
                this.ResetPosition();
            }
        }
    }

    protected virtual void SwipeGestureCallback(TKSwipeRecognizer r)
    {
        if (r.completedSwipeDirection == TKSwipeDirection.Right)
        {
            var appController = FindObjectOfType<AppController>();
            appController.Back();
        }
    }

	private void LogPageView()
	{
		PageViewTelemetry pageViewTelemetry = new PageViewTelemetry(this.gameObject.name);
		pageViewTelemetry.Properties = new Dictionary<string, string>();
		pageViewTelemetry.Properties.Add("game", Screen.width == 1080 ? "flip" : "fold");
		ApplicationInsights.Instance.TrackPageView(pageViewTelemetry);
	}
}
