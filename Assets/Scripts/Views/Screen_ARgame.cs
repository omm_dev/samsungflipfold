using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Screen_ARgame : AnimatedScreen
{

    public TextMeshProUGUI[] m_TextColoured;
    public Image[] m_ImageColour;
    public GameObject m_ImageTarget;
    public TextMeshProUGUI m_timerText;
    [HideInInspector]
    public float timer = 0;
    private bool gameOver = false;
    public GameObject m_ScreenStage_1;
    public GameObject m_ScreenStage_2;
    public GameObject m_ScreenStage_3;
    public AppController m_appController;
    private bool hasGameStarted = false;
    public GameObject hideMaze;

    public override void OnEnterStarted()
    {
        base.OnEnterStarted();
        hasGameStarted = false;
        gameOver = false;
        timer = 0;
        hideMaze.SetActive(true);

        Debug.Log("AR Enter");
        m_ImageTarget.SetActive(true);
        m_ScreenStage_1.SetActive(true);
        m_ScreenStage_2.SetActive(false);
        m_ScreenStage_3.SetActive(false);

    }
    public override void OnExitStarted()
    {
        base.OnExitStarted();
        timer = 0;
        gameOver = true;
        Debug.Log("AR Exited");
        m_ImageTarget.SetActive(false);
        m_ScreenStage_1.SetActive(true);
        m_ScreenStage_2.SetActive(false);
        m_ScreenStage_3.SetActive(false);
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventNames.ResetARGame, ResetGame);
        EventManager.StartListening(EventNames.ResetAll, ResetAll);
        EventManager.StartListening(EventNames.EndGame, GameCompleted);

    }

    private void OnDisable()
    {
        EventManager.StopListening(EventNames.ResetARGame, ResetGame);
        EventManager.StopListening(EventNames.ResetAll, ResetAll);
        //EventManager.StopListening(EventNames.StartGame, StartGame);
        EventManager.StopListening(EventNames.EndGame, GameCompleted);
        if (hideMaze != null)
            hideMaze.SetActive(true);
    }

    private void Start()
    {

        Color CurrentPhoneColorScheme;
        ScreenManager m_screenManager = FindObjectOfType<ScreenManager>();
        if (Screen.width == 1080)
        {
            CurrentPhoneColorScheme = m_screenManager.m_FlipColor;
        }
        else
        {
            CurrentPhoneColorScheme = m_screenManager.m_FoldColor;

        }
        //SETS TEXT COLOURS ON THIS SCREEN
        if (m_TextColoured.Length != 0)
        {
            foreach (TextMeshProUGUI text in m_TextColoured)
            {
                if (Screen.width == 1080)
                {
                    text.color = CurrentPhoneColorScheme;
                }
                else
                {
                    text.color = CurrentPhoneColorScheme;
                }
            }
        }
        //SETS TEXT COLOURS ON THIS SCREEN
        if (m_ImageColour.Length != 0)
        {
            foreach (Image image in m_ImageColour)
            {
                if (Screen.width == 1080)
                {
                    image.color = CurrentPhoneColorScheme;
                }
                else
                {
                    image.color = CurrentPhoneColorScheme;
                }
            }
        }

    }

    public void OnImageTargetDetected()
    {
        if (hasGameStarted == false)
        {
            m_ScreenStage_1.SetActive(false);
            m_ScreenStage_2.SetActive(true);
            m_ScreenStage_3.SetActive(false);
            hasGameStarted = true;

        }
    }

    public void StartMazeButton()
    {
      
        EventManager.TriggerEvent(EventNames.StartGame);
        gameOver = false;
        StartCoroutine(GameTimer());
        m_ScreenStage_2.SetActive(false);
        m_ScreenStage_3.SetActive(true);
        hideMaze.SetActive(false);


    }

    private void ResetGame()
    {
        timer = 0;
        gameOver = false;
    }

    private void ResetAll()
    {
        timer = 0;
        gameOver = true;
        m_ImageTarget.SetActive(false);
        m_ScreenStage_1.SetActive(true);
        m_ScreenStage_2.SetActive(false);
        m_ScreenStage_3.SetActive(false);
    }

   
    private void GameCompleted()
    {
        gameOver = true;
        m_ImageTarget.SetActive(false);
        m_appController.Next();
    }
    private IEnumerator GameTimer()
    {
        while (!gameOver)
        {
            timer += Time.deltaTime;
            //float seconds = Mathf.Round(timer * 100.0f) / 100.0f;
            int minutes = (int)timer / 60;
            int seconds = (int)timer - 60 * minutes;
            int milliseconds = (int)(100 * (timer - minutes * 60 - seconds));


            string display = string.Format("{0:00}:{1:00}:{2:00}", minutes, seconds, milliseconds);
            m_timerText.text = display;
            yield return null;
        }
        gameOver = true;
    }
}
