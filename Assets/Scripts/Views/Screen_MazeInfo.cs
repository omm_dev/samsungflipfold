using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screen_MazeInfo : AnimatedScreen
{

    public GameObject demoAnimation;
    private Animator animator;

    private void Awake()
    {
        if (demoAnimation == null) return;
        animator = demoAnimation.GetComponent<Animator>();
        demoAnimation.SetActive(false);
    }


    public override void OnEnterStarted()
    {
        base.OnEnterStarted();
        if (animator == null) return;
        if (demoAnimation == null) return;

        demoAnimation.SetActive(true);
        animator.SetBool("isAnimating", true);
    }

    public override void OnExitEnded()
    {
        base.OnExitEnded();
        if (animator == null) return;
        if (demoAnimation == null) return;

        demoAnimation.SetActive(false);
        animator.SetBool("isAnimating", false);

    }
}
