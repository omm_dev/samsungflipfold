using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pixelplacement;
using UnityEngine.EventSystems;

public class Component_Leaderboard : MonoBehaviour
{


    public RectTransform contentPanel;
    public Component_LeaderboardItem leaderBoardPrefab;
    public float time;
    public AnimationCurve animationCurve;
    public int numFakeItems;
    public Scrollbar scrollbar;

    private ScrollRect scrollRect;
    private RectTransform target;
    private List<Component_LeaderboardItem> leaderBoardItemList = new List<Component_LeaderboardItem>();
    private List<Component_LeaderboardItem> fakeBoardItemList = new List<Component_LeaderboardItem>();

    public void Setup(RankInfo[] rankInfo)
    {
        if (leaderBoardPrefab == null) return;
        if (contentPanel == null) return;
        if (scrollbar == null) return;
        if (rankInfo == null) return;
        if (rankInfo.Length <= 0) return;

        scrollRect = gameObject.GetComponent<ScrollRect>();
        if (scrollRect == null) return;

        for (int i = 0; i < numFakeItems; i++)
        {
            Component_LeaderboardItem component_LeaderboardItem = Instantiate<Component_LeaderboardItem>(leaderBoardPrefab, contentPanel);
            component_LeaderboardItem.Setup(rankInfo[i % rankInfo.Length]);
            fakeBoardItemList.Add(component_LeaderboardItem);
        }



        for (int i = 0; i < rankInfo.Length; i++)
        {
            Component_LeaderboardItem component_LeaderboardItem = Instantiate<Component_LeaderboardItem>(leaderBoardPrefab, contentPanel);
            component_LeaderboardItem.Setup(rankInfo[i % rankInfo.Length]);
            leaderBoardItemList.Add(component_LeaderboardItem);
        }

        if (fakeBoardItemList.Count <= 0) return;
        ScrollTo(fakeBoardItemList[0].GetComponent<RectTransform>());

    }

    public void StartAnimation(int arrayPosition)
    {
        if (leaderBoardItemList.Count <= arrayPosition) return;
        SetActiveButtons(false);
        SetTarget(leaderBoardItemList[arrayPosition]);
        TweenTo(target);
    }


    private void TweenTo(RectTransform target)
    {
        Canvas.ForceUpdateCanvases();
        //System.Action<string> hi = HandleTweenFinished;
        Tween.AnchoredPosition(contentPanel, GetTargetPosition(contentPanel, target), time, 0.5f, animationCurve, Tween.LoopType.None,null, HandleTweenFinished);
    }

    private void ScrollTo(RectTransform target)
    {
        Canvas.ForceUpdateCanvases();

        contentPanel.anchoredPosition = GetTargetPosition(contentPanel, target);
    }

    private Vector2 GetTargetPosition(RectTransform contentPanel, RectTransform target)
    {
        return (Vector2)scrollRect.transform.InverseTransformPoint(contentPanel.position) - (Vector2)scrollRect.transform.InverseTransformPoint(target.position) - new Vector2(0.0f,gameObject.GetComponent<RectTransform>().rect.height *0.5f);
    }

    private void SetTarget(Component_LeaderboardItem component)
    {
        target = component.gameObject.GetComponent<RectTransform>();
        component.gameObject.GetComponent<Component_LeaderboardItem>().SelectItem();
    }

    private void clearFakeItems()
    {
        for (int i = 0; i < fakeBoardItemList.Count; i++)
        {
            fakeBoardItemList[i].gameObject.SetActive(false);
            Destroy(fakeBoardItemList[i]);
        }

        fakeBoardItemList.Clear();

    }

    void HandleTweenFinished()
    {
        clearFakeItems();
         ScrollTo(target);
        scrollRect.verticalScrollbar = scrollbar;
        SetActiveButtons(true);
    }

    void SetActiveButtons(bool active)
    {
        GameObject screen = GameObject.Find("Leaderboard_Screen");
        if (screen == null) return;
        Button[] buttons = screen.GetComponentsInChildren<Button>();
        foreach (Button b in buttons)
            b.interactable = active;
    }

}


