using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Component_FieldsGroup : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        if (rectTransform == null) return;

        rectTransform.offsetMin = new Vector2(IsFlip() ? 100 : 500, rectTransform.offsetMin.y);
        rectTransform.offsetMax = new Vector2(IsFlip() ? -100 : -500, rectTransform.offsetMax.y);
    }

    bool IsFlip()
    {
        return Screen.width == 1080;
    }


}
