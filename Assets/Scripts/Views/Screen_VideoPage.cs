using RenderHeads.Media.AVProVideo;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Screen_VideoPage : AnimatedScreen
{
    public TextMeshProUGUI[] m_TextColoured;
    public Image[] m_ImageColour;
    public GameObject m_FlipVideoPage;
    public GameObject m_FoldVideoPage;
    public MediaPlayer m_FlipVideoPlayer;
    public string m_FlipPath;
    public MediaPlayer m_FoldVideoPlayer;
    public string m_FoldPath;
    public AppController m_appController;

    public override void OnEnterStarted()
    {
        base.OnEnterStarted();

        PlayVideo();
    }
    public override void OnExitEnded()
    {
        base.OnExitEnded();
        m_FlipVideoPlayer.CloseMedia();

        m_FoldVideoPlayer.CloseMedia();
    }



    private void Start()
    {

        //m_appController = FindObjectOfType<AppController>();
        SetVideo();

        Color CurrentPhoneColorScheme;
        ScreenManager m_screenManager = FindObjectOfType<ScreenManager>();
        if (Screen.width == 1080)
        {
            CurrentPhoneColorScheme = m_screenManager.m_FlipColor;
        }
        else
        {
            CurrentPhoneColorScheme = m_screenManager.m_FoldColor;
        }
        //SETS TEXT COLOURS ON THIS SCREEN
        if (m_TextColoured.Length != 0)
        {
            foreach (TextMeshProUGUI text in m_TextColoured)
            {
                if (Screen.width == 1080)
                {
                    text.color = CurrentPhoneColorScheme;
                }
                else
                {
                    text.color = CurrentPhoneColorScheme;
                }
            }
        }
        //SETS TEXT COLOURS ON THIS SCREEN
        if (m_ImageColour.Length != 0)
        {
            foreach (Image image in m_ImageColour)
            {
                if (Screen.width == 1080)
                {
                    image.color = CurrentPhoneColorScheme;
                }
                else
                {
                    image.color = CurrentPhoneColorScheme;
                }
            }
        }

    }
    public void Awake()
    {
        SetVideo();
    }
    public void SetVideo()
    {
        if (Screen.width == 1080)
        {
            m_FlipVideoPage.SetActive(true);
            m_FoldVideoPage.SetActive(false);
        }
        else
        {
            m_FlipVideoPage.SetActive(false);
            m_FoldVideoPage.SetActive(true);
        }
    }
    public void PlayVideo()
    {
        if (Screen.width == 1080)
        {
            m_FlipVideoPlayer.OpenMedia(MediaPathType.RelativeToStreamingAssetsFolder, m_FlipPath, false);

            m_FlipVideoPlayer.Play();

        }
        else
        {
            m_FoldVideoPlayer.OpenMedia(MediaPathType.RelativeToStreamingAssetsFolder, m_FoldPath, false);
            m_FoldVideoPlayer.Play();

        }
    }
    public void CloseVideoPlayer()
    {
        if (Screen.width == 1080)
        {

            m_FlipVideoPlayer.CloseMedia();

        }
        else
        {
            m_FoldVideoPlayer.CloseMedia();

        }
    }
    public void VideoFinished()
    {
        m_appController.Next();
    }
}
