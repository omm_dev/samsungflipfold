using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class Screen_SignUp : AnimatedScreen
{

    public List<GameObject> coverList;

    // Start is called before the first frame update
    void Start()
    {
        if (coverList.Count < 2) return;
        coverList[Convert.ToInt32(IsFlip())].SetActive(true);

    }



    bool IsFlip()
    {
        return Screen.width == 1080;
    }

    public override void OnEnterStarted()
    {
        base.OnEnterStarted();
        Form_SignUp form_signUp = GetComponent<Form_SignUp>();
        if (form_signUp == null) return;
        form_signUp.ResetForm();
    }

 
}
