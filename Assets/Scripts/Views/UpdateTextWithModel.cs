using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;



public class UpdateTextWithModel : MonoBehaviour
{
    // Start is called before the first frame update
    public string placeHolder = "{model}";

    void Awake()
    {
        TextMeshProUGUI writableComponent = GetComponent<TextMeshProUGUI>();
        if (writableComponent == null) return;

        writableComponent.text =  writableComponent.text.Replace(placeHolder, Screen.width == 1080 ? "Flip3" : "Fold3");
    }
}
