using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Screen_Congrats : AnimatedScreen
{

    public TextMeshProUGUI[] m_TextColoured;
    public Image[] m_ImageColour;
    public TextMeshProUGUI m_timerText;
    public Screen_ARgame m_ScreenARgame;

    public override void OnEnterStarted()
    {
        base.OnEnterStarted();
        float time = m_ScreenARgame.timer;
        int minutes = (int)time / 60;
        int seconds = (int)time - 60 * minutes;
        int milliseconds = (int)(100 * (time - minutes * 60 - seconds));
         

        string display = string.Format("{0:00}:{1:00}:{2:00}", minutes, seconds, milliseconds);

        m_timerText.text = display;
    }
    

    

    private void Start()
    {

        Color CurrentPhoneColorScheme;
        ScreenManager m_screenManager = FindObjectOfType<ScreenManager>();
        if (Screen.width == 1080)
        {
            CurrentPhoneColorScheme = m_screenManager.m_FlipColor;
        }
        else
        {
            CurrentPhoneColorScheme = m_screenManager.m_FoldColor;

        }
        //SETS TEXT COLOURS ON THIS SCREEN
        if (m_TextColoured.Length != 0)
        {
            foreach (TextMeshProUGUI text in m_TextColoured)
            {
                if (Screen.width == 1080)
                {
                    text.color = CurrentPhoneColorScheme;
                }
                else
                {
                    text.color = CurrentPhoneColorScheme;
                }
            }
        }
        //SETS TEXT COLOURS ON THIS SCREEN
        if (m_ImageColour.Length != 0)
        {
            foreach (Image image in m_ImageColour)
            {
                if (Screen.width == 1080)
                {
                    image.color = CurrentPhoneColorScheme;
                }
                else
                {
                    image.color = CurrentPhoneColorScheme;
                }
            }
        }

    }

    
}
