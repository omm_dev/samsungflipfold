using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Component_LeaderboardItem : MonoBehaviour
{
    // Start is called before the first frame update
    public TextMeshProUGUI rankText;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI timeText;

    public void SelectItem()
    {
        if (rankText == null) return;
        gameObject.GetComponent<Image>().enabled = true;
        rankText.color = Color.white;
    }

    public void Setup(RankInfo info)
    {
        if (rankText == null) return;
        if (nameText == null) return;
        if (timeText == null) return;

        rankText.text = info.rank.ToString();
        nameText.text = info.leaderboardName;
        timeText.text = info.time.ToString();
    }
}
