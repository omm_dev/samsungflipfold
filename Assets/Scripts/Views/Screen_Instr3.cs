using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Screen_Instr3 : AnimatedScreen
{

    public TextMeshProUGUI[] m_TextColoured;
    public Image[] m_ImageColour;
    public GridLayoutGroup m_gridLayout;
    [Space(10)]
    [Header("Flip Size Parameters for Grid")]
    public float m_FlipGridCellWidth;
    
    [Space(10)]
    [Header("Fold Size Parameters for Grid")]
    public float m_FoldGridCellWidth;
   
    private void Start()
    {
        Color CurrentPhoneColorScheme;
        ScreenManager m_screenManager = FindObjectOfType<ScreenManager>();
        if (Screen.width == 1080)
        {
            CurrentPhoneColorScheme = m_screenManager.m_FlipColor;
            m_gridLayout.cellSize =new Vector2(m_FlipGridCellWidth,m_gridLayout.cellSize.y);
        }
        else
        {
            CurrentPhoneColorScheme = m_screenManager.m_FoldColor;
            m_gridLayout.cellSize = new Vector2(m_FoldGridCellWidth, m_gridLayout.cellSize.y);
        }
        //SETS TEXT COLOURS ON THIS SCREEN
        if (m_TextColoured.Length != 0)
        {
            foreach (TextMeshProUGUI text in m_TextColoured)
            {
                if (Screen.width == 1080)
                {
                    text.color = CurrentPhoneColorScheme;
                }
                else
                {
                    text.color = CurrentPhoneColorScheme;
                }
            }
        }
        //SETS TEXT COLOURS ON THIS SCREEN
        if (m_ImageColour.Length != 0)
        {
            foreach (Image image in m_ImageColour)
            {
                if (Screen.width == 1080)
                {
                    image.color = CurrentPhoneColorScheme;
                }
                else
                {
                    image.color = CurrentPhoneColorScheme;
                }
            }
        }

    }
}
