using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TintMe : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        Graphic tintableComponent = GetComponent<Graphic>();
        if (tintableComponent == null) return;

        ScreenManager m_screenManager = FindObjectOfType<ScreenManager>();
        if (m_screenManager == null) return;

        tintableComponent.color = Screen.width == 1080 ? m_screenManager.m_FlipColor : m_screenManager.m_FoldColor;
    }
}
