using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum PowerUpType
{
    // General
    Clockwise = 0,
    Anyclockwise = 1,
    Block = 2,
    Spring = 3,
    Levitate = 4
   
}

public class Component_TileDemo : MonoBehaviour
{

public PowerUpType   powerUpType;
    public List<Sprite> powerUpSprite;
    public List<string> powerUpText;
    // Start is called before the first frame update

    private void Start(){
        if (powerUpSprite.Count <= (int)powerUpType) return;
        if (powerUpText.Count <= (int)powerUpType) return;


        transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = powerUpText[(int)powerUpType];
        transform.GetChild(1).gameObject.GetComponent<Image>().sprite = powerUpSprite[(int)powerUpType];
        //SetLayerRecursively(this.gameObject, 9);


    }

    void SetLayerRecursively(GameObject obj, int newLayer)
    {
        if (null == obj)
        {
            return;
        }

        obj.layer = newLayer;

        foreach (Transform child in obj.transform)
        {
            if (null == child)
            {
                continue;
            }
            SetLayerRecursively(child.gameObject, newLayer);
        }
    }

}
