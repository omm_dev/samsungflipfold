using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Networking;

[System.Serializable]
public class RankInfo
{
    public int userId;
    public string leaderboardName;
    public float time;
    public int rank;
}

public class Form_SignUp : MonoBehaviour
{
    public TMP_InputField firstNameInput;
    public TMP_InputField lastNameInput;
    public TMP_InputField emailInput;
    public TMP_InputField leaderboardNameInput;
    public Toggle majorityToggle;
    public Toggle leaderboardPermissionToggle;
    public Toggle galaxyFlipGamePermissionToggle;
    public Toggle samsungNotificationPermissionToggle;
    public Button submitButton;
    public Component_ErrorOverlay errorOverlay;

    private AppController appController;
    private FormData formData;
    private const string API_URL = "https://samsungflipfoldapi.azure-api.net";
    private const int MIN_CHAR_FIELD = 1;

    public struct FormData
    {
        public string firstName;
        public string lastName;
        public string email;
        public string leaderboardName;
        public float time;
        public bool leaderboardPermission;
        public bool galaxyFlipGamePermission;
        public bool samsungNotificationPermission;
        public bool isFlip;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (submitButton == null) return;
        submitButton.onClick.AddListener(Submit);

        appController = GetAppController();
    }

    void Submit()
    {
        if (FormIsValid())
        {
            SetFormData();
            StartCoroutine(SendForm(formData));
        }
    }

    bool MailIsValid(TMP_InputField email)
    {

        if (email == null) return false;

        try
        {
            var addr = new System.Net.Mail.MailAddress(email.text);
            return addr.Address == email.text;
        }
        catch
        {
            ShowError("Mail not valid");
            return false;
        }
    }

    bool TextFieldIsValid(TMP_InputField inputField)
    {
        bool valid = false;
        if (inputField == null) return valid;
        valid = inputField.text.Length >= MIN_CHAR_FIELD && inputField.text.Length < 11;

        if (!valid) ShowError(inputField.gameObject.name + " length not valid.");

        return valid;
    }


    bool ToggleIsChecked(Toggle toggle)
    {
        bool valid = false;
        if (toggle == null) return valid;
        valid = toggle.isOn;

        if (!valid && toggle.gameObject.name.Contains("majority"))
        {
            ShowError("You must be over 13 years to submit this data.");
        }

        return valid;
    }

    bool FormIsValid()
    {


        return TextFieldIsValid(firstNameInput) &&
            TextFieldIsValid(lastNameInput) &&
            MailIsValid(emailInput) &&
            TextFieldIsValid(leaderboardNameInput) &&
            ToggleIsChecked(majorityToggle);

    }

    void SetFormData()
    {
        FormData fD = new FormData();
        fD.firstName = firstNameInput.text;
        fD.lastName = lastNameInput.text;
        fD.email = emailInput.text;
        fD.leaderboardName = leaderboardNameInput.text;
        fD.time = GetPlayerTime();
        fD.leaderboardPermission = leaderboardPermissionToggle.isOn;
        fD.galaxyFlipGamePermission = galaxyFlipGamePermissionToggle.isOn;
        fD.samsungNotificationPermission = samsungNotificationPermissionToggle.isOn;
        fD.isFlip = IsFlip();

        formData = fD;
    }

    float GetPlayerTime()
    {
        Screen_ARgame screenArGame = GetScreenArGame();
        if (screenArGame == null) return 0.0f;
        return screenArGame.timer;
    }

    AppController GetAppController()
    {

        GameObject stateMachine = GameObject.Find("/StateMachine");
        if (stateMachine == null) return null;

        return stateMachine.GetComponent<AppController>();

    }

    Screen_Leaderboard GetLeaderboard()
    {

        GameObject leaderboard = GameObject.Find("UI_Canvas/Leaderboard_Screen");
        if (leaderboard == null) return null;

        return leaderboard.GetComponent<Screen_Leaderboard>();

    }

    Screen_ARgame GetScreenArGame()
    {
        GameObject arGameUI = GameObject.Find("/UI_Canvas/ARGameUI");
        if (arGameUI == null) return null;
        
        return arGameUI.GetComponent<Screen_ARgame>();
    }


    IEnumerator SendForm(FormData data)
    {

        WWWForm form = new WWWForm();
        form.AddField("firstName", data.firstName);
        form.AddField("lastName", data.lastName);
        form.AddField("email", data.email);
        form.AddField("leaderboardName", data.leaderboardName);
        form.AddField("time", GetPlayerTime().ToString());
        form.AddField("leaderboardPermission", data.leaderboardPermission.ToString());
        form.AddField("galaxyFlipGamePermission", data.galaxyFlipGamePermission.ToString());
        form.AddField("samsungNotificationPermission", data.samsungNotificationPermission.ToString());
        form.AddField("isFlip", data.isFlip.ToString());

        UnityWebRequest www = UnityWebRequest.Post(API_URL + "/api/User", form);
        www.SetRequestHeader("Ocp-Apim-Subscription-Key", "6a1575618a584644b6f0b75801cf0562");
        SetActiveButtons(false);
        yield return www.Send();
        SetActiveButtons(true);

        if (www.isNetworkError)
        {
            Debug.Log(www.error);
            ShowError("Server error.");
            www.Dispose();
        }
        else
        {
            if (HttpHelper.IsSuccess(www)){
                //if (data.leaderboardPermission)
                //{
                //    StartCoroutine(GetRank());
                //}
                //else 
                if (appController != null)
                {
                    appController.ResetApp();
                }

                Debug.Log(www.downloadHandler.text);
                www.Dispose();
            }
            else
            {
                Debug.Log("/User failed");
                ShowError("Server error.");
                www.Dispose();
            }


        }
    }



    IEnumerator GetRank()
    {
        string req = API_URL + "/api/Leaderboard/ByEmail?email=" + formData.email + "&NumberOfResults=20";
        SetActiveButtons(false);
        UnityWebRequest www = UnityWebRequest.Get(req);
        www.SetRequestHeader("Ocp-Apim-Subscription-Key", "6a1575618a584644b6f0b75801cf0562");
        yield return www.Send();

        if (www.isNetworkError)
        {
            ShowError("Server error.");
            Debug.Log(www.error);
            SetActiveButtons(true);
            www.Dispose();
        }
        else
        {
            if (HttpHelper.IsSuccess(www))
            {
                Debug.Log(www.downloadHandler.text);
                RankInfo[] rankInfo = JsonHelper.FromJson<RankInfo>(JsonHelper.FixJson(www.downloadHandler.text));
                Screen_Leaderboard cL = GetLeaderboard();
                if (cL != null)
                {
                    cL.Setup(rankInfo, GetArrayPosition(rankInfo, formData.leaderboardName));
                    
                }

                if (appController != null)
                {
                    appController.Next();   
                }
                SetActiveButtons(true);
                www.Dispose();
            }
            else
            {
                Debug.Log("/ByEmail failed");
                ShowError("Server error.");
                SetActiveButtons(true);
                www.Dispose();
            }
               
            
            
        }
    }

    int GetArrayPosition(RankInfo[] rankInfo, string leaderBoardName)
    {
        for(int i=0; i < rankInfo.Length; i++)
        {
            if(rankInfo[i].leaderboardName == leaderBoardName)
            {
                return i;
            }
        }
        return 0;
    }

    void ShowError(string error)
    {
        if (errorOverlay == null) return;
        errorOverlay.ShowError(error);
    }

    void SetActiveButtons(bool active)
    {
        Button[] buttons = GetComponentsInChildren<Button>();
        foreach (Button b in buttons)
            b.interactable = active;
    }

    public void ResetForm()
    {
        TMP_InputField[] textfields = GetComponentsInChildren<TMP_InputField>();
        foreach (TMP_InputField tf in textfields)
            tf.text = "";

        Toggle[] toggles = GetComponentsInChildren<Toggle>();
        foreach (Toggle t in toggles)
            t.isOn = false;
    }

    bool IsFlip()
    {
        return Screen.width == 1080;
    }

}


public static class HttpHelper
{
    public static bool IsSuccess(this UnityWebRequest request)
    {
        if (request.isNetworkError) { return false; }

        if (request.responseCode == 0) { return true; }
        if (request.responseCode == (long)System.Net.HttpStatusCode.OK) { return true; }

        return false;
    }
}

public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    public static string FixJson(string value)
    {
        value = "{\"Items\":" + value + "}";
        return value;

    }

    [System.Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }

}
