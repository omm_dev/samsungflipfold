using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Component_ErrorOverlay : MonoBehaviour
{
    // Start is called before the first frame update

    public TextMeshProUGUI errorText;

    public void ShowError(string errorString)
    {
        if (errorText == null) return;
        gameObject.SetActive(true);
        errorText.text = errorString;
    }

    public void HideOverlay()
    {
        gameObject.SetActive(false);
    }
    
}
