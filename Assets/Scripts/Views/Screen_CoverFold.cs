﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Screen_CoverFold : MonoBehaviour
{

    public GameObject m_FoldCover;
    private void Start()
    {
        //Listen for screen size change
        WindowManager.instance.ScreenSizeChangeEvent += Instance_ScreenSizeChangeEvent;
        if (Screen.width < 1080)
        {
            foreach (Transform child in this.transform)
            {
                m_FoldCover.SetActive(true);
            }
        }
        else
        {
            m_FoldCover.SetActive(false);
        }

    }
    private void Instance_ScreenSizeChangeEvent(int Width, int Height)
    {
        Debug.Log("Screen Size Width = " + Width.ToString() + "  Height = " + Height.ToString());
        if (Width < 1080)
        {
            foreach (Transform child in this.transform)
            {
                m_FoldCover.SetActive(true);
            }
        }
        else
        {
            m_FoldCover.SetActive(false);
        }
    }
}
