using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;



// Class to debug the Game, it will not be part of the final build
public class DebugScript : MonoBehaviour
{
    public AppController m_appController;

    [Space(10)]
    public bool m_changeCameraAngle = false;
    public bool m_skipToARScene = false;

    public KeyCode m_skipGameKey;

    public KeyCode m_triggertargetImageBottomLeft;
    public UnityEvent OnTargetFoundImageBottomLeft;
   
    public KeyCode m_triggertargetImageTopLeft;
    public UnityEvent OnTargetFoundImageTopLeft;
   
    public KeyCode m_triggertargetImageCentral;
    public UnityEvent OnTargetFoundImageCentral;

    public KeyCode m_triggertargetImageBottomRight;
    public UnityEvent OnTargetFoundImageBottomRight;

    public KeyCode m_triggertargetImageTopRight;
    public UnityEvent OnTargetFoundImageTopRight;

    
#if UNITY_EDITOR


    // Update is called once per frame
    void Update()
    {
       if (m_skipToARScene == true)
        {
            if ( m_appController.CurrentState != AppState.ARgame)
            {
                m_appController.Next();
                return;
            }
            m_skipToARScene = false;
        }
       if (m_changeCameraAngle == true)
        {
            Camera.main.transform.position = new Vector3(0.8f, 3, 0.45f);
            Camera.main.transform.eulerAngles = new Vector3(90, 0, 0);
            m_changeCameraAngle = false;
           // OnTargetFoundImageBottomLeft.Invoke();
        }
       if (Input.GetKeyDown(m_triggertargetImageBottomLeft))
        {

            Camera.main.transform.position = new Vector3(0.8f, 3, 0.45f);
            Camera.main.transform.eulerAngles = new Vector3(90, 0, 0);
            OnTargetFoundImageBottomLeft.Invoke();
        }

        if (Input.GetKeyDown(m_skipGameKey))
        {
            EventManager.TriggerEvent(EventNames.EndGame);
        }
        if (Input.GetKeyDown(m_triggertargetImageTopLeft))
        {
            OnTargetFoundImageTopLeft.Invoke();
        }
        if (Input.GetKeyDown(m_triggertargetImageCentral))
        {
            OnTargetFoundImageCentral.Invoke();
        }
        if (Input.GetKeyDown(m_triggertargetImageBottomRight))
        {
            OnTargetFoundImageBottomRight.Invoke();
        }
        if (Input.GetKeyDown(m_triggertargetImageTopRight))
        {
            OnTargetFoundImageTopRight.Invoke();
        }
    }


#endif
}
