using System.Collections;
using System.Collections.Generic;
using UnityEngine;



// Class to reposition Camera for Debug purpose
// Do not add to final build
public class DebugRepositionCamera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            this.transform.position = new Vector3(0.8f, 8, 0);
            this.transform.eulerAngles = new Vector3(90, 0, 0);
            EventManager.TriggerEvent(EventNames.StartGame);
        }
    }
}
