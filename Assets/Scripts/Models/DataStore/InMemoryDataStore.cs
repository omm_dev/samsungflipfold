﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Models.DataStore
{
	public sealed class InMemoryDataStore
	{
		private static InMemoryDataStore _instance = null;
		private static readonly object padlock = new object();

		InMemoryDataStore()
		{
		}

		public static InMemoryDataStore Instance
		{
			get
			{
				lock (padlock)
				{
					if (_instance == null)
					{
						_instance = new InMemoryDataStore();
					}
					return _instance;
				}
			}
		}
		public string CurrentStoreId { get; set; }
		public int NumVideosRecorded { get; set; }
		public string UserId { get; set; } = "user";
	}
}
